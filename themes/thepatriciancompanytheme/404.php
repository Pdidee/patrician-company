<?php
/**
 * 404 Page
 * Now with added GIF!
 */

get_header();
?>

<div class="container">

	<?
		$target_post_id = "5";
		if (has_post_thumbnail($target_post_id) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id($target_post_id), 'full' ); ?>
			<?php endif; ?>
	<div class="featured-image" style="background-image:url('<?php echo $image[0]; ?>');">
		<div class="overlay"></div>
		<h1><?php the_field('featured_title', 5); ?></h1>
		<div class="arrowdown"><img src="<?php echo bloginfo('template_url'); ?>/_static/images/arrowdown.png" width="30px"></div>
	</div>

	
	<div class="header">
		<h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.' ); ?></h1>
	</div>


</div>

<?php get_footer(); ?>
