<?php
/**
 * Register [custom_post_type_name]
 */
/*function aa_register_custom_post_type_name() {
    $labels = array(
        'name' => _x( 'Eventbook', 'custom_post_type_name' ),
        'singular_name' => _x( 'Eventbook', 'custom_post_type_name' ),
        'add_new' => _x( 'Add New', 'custom_post_type_name' ),
        'add_new_item' => _x( 'Add New Eventbook', 'custom_post_type_name' ),
        'edit_item' => _x( 'Edit Eventbook', 'custom_post_type_name' ),
        'new_item' => _x( 'New Eventbook', 'custom_post_type_name' ),
        'view_item' => _x( 'View Eventbook', 'custom_post_type_name' ),
        'search_items' => _x( 'Search Eventbook', 'custom_post_type_name' ),
        'not_found' => _x( 'No Eventbook', 'custom_post_type_name' ),
        'not_found_in_trash' => _x( 'No Eventbookfound in Trash', 'custom_post_type_name' ),
        'parent_item_colon' => _x( 'Parent Eventbook:', 'custom_post_type_name' ),
        'menu_name' => _x( 'Eventbook', 'custom_post_type_name' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,

        'supports' => array( 'title', 'thumbnail', 'editor' ),
        'taxonomies' => array('post_tag'),

        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-welcome-widgets-menus',

        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'eventbook', $args );
}
add_action( 'init', 'aa_register_custom_post_type_name' );

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM POST TYPE REGISTRATION
/*-----------------------------------------------------------------------------------*/

function eventbook_init() {
    $args = array(
      'label' => 'Eventbook',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
		'has_archive' => true,
        'rewrite' => array('slug' => 'eventbook'),
        'query_var' => true,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array(
            'title',
            'editor',
            'revisions',
            'thumbnail',
            'author',)
        );
    register_post_type( 'eventbook', $args );
}
add_action( 'init', 'eventbook_init' );


/*-----------------------------------------------------------------------------------*/
/*  CUSTOM TAXONOMIES
/*-----------------------------------------------------------------------------------*/

function eventbook_taxonomies() {
  $labels = array(
    'name'              => _x( 'Eventbook Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Eventbook Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Eventbook Categories' ),
    'all_items'         => __( 'All Eventbook Categories' ),
    'parent_item'       => __( 'Parent Eventbook Category' ),
    'parent_item_colon' => __( 'Parent Eventbook Category:' ),
    'edit_item'         => __( 'Edit Eventbook Category' ), 
    'update_item'       => __( 'Update Eventbook Category' ),
    'add_new_item'      => __( 'Add New Eventbook Category' ),
    'new_item_name'     => __( 'New Eventbook Category' ),
    'menu_name'         => __( 'Eventbook Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'eventbook_category', 'eventbook', $args );
}
add_action( 'init', 'eventbook_taxonomies', 0 );


/* THIS HELPS FOR SINGLE PAGES FOR CUSTOM POST TYPES */
flush_rewrite_rules();
?>