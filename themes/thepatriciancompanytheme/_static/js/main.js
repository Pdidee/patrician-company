/**
 * Main JS file where all custom JS is done
 */
(function($) {
"use strict";
	$(document).ready( function() {
		function animate() {
	    $('.arrowdown').fadeTo(700, 1).delay(100).fadeTo(700, 0);
	}
	setInterval(animate, 1000);



// to top right away
if ( window.location.hash ) scroll(0,0);
// void some browsers issue
setTimeout( function() { scroll(0,0); }, 1);

$(function() {
    // *only* if we have anchor on the url
    if(window.location.hash) {
        // smooth scroll to the anchor id
        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top + 'px'
        }, 1000, 'swing');
    }
});

$(".page-home .menu-item-16 a").click(function(event) {
	event.preventDefault();
	$('html, body').animate({
	    scrollTop: $("#services").offset().top -50
	}, 2000);
});


	$(".arrowdown").click(function(event) {
		event.preventDefault();
		$('html, body').animate({
		    scrollTop: $(".inner-wrap").offset().top
		}, 2000);
	});
	 $(function () {
		    // cache the sliding object in a var
		    var slideout = $('#slideout');
		    $(".open").toggle(function () {
		        // use cached object instead of searching
		        $('.open').addClass('active');
		        slideout.animate({
		            right: '0px'
		        }, {
		            queue: false,
		            duration: 200
		        });
		    }, function () {
		        // use cached object instead of searching  
		         $('.open').removeClass('active');      
		        slideout.animate({
		            right: '-350px'
		        }, {
		            queue: false,
		            duration: 200
		        });
		    });
		});

	 $('.gchoice_1_4_1 input').click(function(){
	    $('.gchoice_1_4_1 label').toggleClass('selected');
	}); 
	  $('.gchoice_1_4_2 input').click(function(){
	    $('.gchoice_1_4_2 label').toggleClass('selected');
	}); 
	   $('.gchoice_1_4_3 input').click(function(){
	    $('.gchoice_1_4_3 label').toggleClass('selected');
	}); 
	    $('.gchoice_1_4_4 input').click(function(){
	    $('.gchoice_1_4_4 label').toggleClass('selected');
	}); 

	var targetOffset = $("#container").offset().top;

	var $w = $(window).scroll(function(){
    if ( $w.scrollTop() > targetOffset ) {   
        $('.open').css({"background":"#235b60"});
    } else {
      // ...
    }
});
	 
	}); // document.ready()

})(jQuery);

jQuery(function ($) {
	"use strict";
		//Add fancybox class to images
		$(".gallery a").addClass("fancybox-thumb");
		$(".gallery a").attr("rel", "fancybox-thumb");
	});	
jQuery(function ($) {
	"use strict";
	$(".fancybox-thumb").fancybox({
		'nextEffect': 'fade',
    	'prevEffect': 'fade',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 100,
				height	: 50
			}
		}
	});
});