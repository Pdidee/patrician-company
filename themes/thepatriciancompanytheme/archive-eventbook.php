<?php
/**
 * The template for displaying archive pages.
 */

get_header();
?>

<div class="container">
		<?
		$target_post_id = "109";
		if (has_post_thumbnail($target_post_id) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id($target_post_id), 'full' ); ?>
			<?php endif; ?>
	<div class="featured-image" style="background-image:url('<?php echo $image[0]; ?>');">
		<div class="overlay"></div>
		<h1><?php the_field('featured_title', 109); ?></h1>
		<div class="arrowdown"><img src="<?php echo bloginfo('template_url'); ?>/_static/images/arrowdown.png" width="30px"></div>
	</div>

	<div class="inner-wrap"></div>
	<? $query = new WP_Query( array( 'post_type' => 'eventbook', 'posts_per_page' => -1, 'paged' => $paged ) );
		if ( $query->have_posts() ) : ?>
	<div class="event-listing">
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
			$image = $image[0];?>
			<a href="<? the_permalink(); ?>">
				<div class="singleImage" style="background-image: url('<?php echo $image; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
					<div class="overlay">
						<div class="title"><?php the_title(); ?></div>
					</div>
				</div> 
			</a>
			<?php endwhile; endif;
			wp_reset_query();
			?>
	</div>

</div><!-- .content-area -->


<div class="clearfix"></div>

<?php get_footer(); ?>
<? if ($switch){ ?>
<? } else { ?>
<? } ?>
