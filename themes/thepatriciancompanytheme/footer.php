<footer class="site-footer">
 	 <div class="footer-cta">
		<?php if( get_field('footer_cta', 5) ): ?>
			<?php while( has_sub_field('footer_cta', 5) ): ?>
				<h4><?php the_sub_field('title'); ?></h4>
				<a href="<? the_sub_field('link_to');?>" class="contact"><? the_sub_field('text');?></a>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>	

	<ul class="social">
		<?php if( get_field('social_links', 'option') ): ?>
				<?php while( has_sub_field('social_links', 'option') ): ?>

				<li><a href="<? the_sub_field('social_link');?>" target="_blank"><i class="fa <?php the_sub_field('social_media_icon'); ?>" aria-hidden="true"></i></a></li>

			<?php endwhile; ?>
		<?php endif; ?>
	</ul>

</footer>

<?php wp_footer(); ?>

</body>
</html>
