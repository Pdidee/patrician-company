<?php
/**
 * The various functions are split up and included in the following 3 files.
 * This file is otherwise kept (mostly) bare for you to add your custom functions here.
 */

/**
 * Include optional functions common to all projects
 */
include_once('_includes/common-functions.php');
/**
 * Include Custom Post Types
 */
include_once("_includes/custom-post-types.php");
/**
 * Include scripts and stylesheets from JS plugins
 */
include_once('_includes/enqueue-scripts.php');
/**
 * Includes for admin customisation
 */
include_once('_includes/admin/dashboard-widgets.php' ); // Add custom tweaks to the adman dashboard on the back end.
include_once('_includes/admin/welcome-panel.php' ); // Add a custom welcome panel on the back end.

/**
 * Register ACF Options page and base theme fields
 */
if( function_exists('acf_add_options_page') ) {
	$user = wp_get_current_user();
	$user = $user->user_login;

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	if( $user == 'alchemyandaim' ) :
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Developer Settings',
		'menu_title'	=> 'Developer',
		'parent_slug'	=> 'theme-general-settings',
	));
	endif;
}
require_once( get_template_directory() . '/_includes/acf-base.php' );


/**
 * Define some global variables
 * @var string $images_dir 		Directory for the theme images
 */
$images_dir = get_bloginfo('template_directory') . '/_static/images';

show_admin_bar(false);
/**
 * Customize admin login logo
 */
function aa_custom_login_logo() {
	echo '<style type="text/css">
	h1 a {
		background-image:url('. get_template_directory_uri() .'/_static/images/patricianlogo2.png) !important;
		padding-bottom: 0px !important;
		margin-bottom: 0 !important;
		background-size: 260px !important;
		height: 80px !important;
		width: 100% !important;
	}
	#loginform .button-primary {
		border-color: #bbbdc0;
		background: #bbbdc0;
	}
	</style>';
}
add_action('login_head', 'aa_custom_login_logo');
