<?php
/**
 * Default Page Template
 */

get_header();
?>

<div class="container">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
		<?php endif; ?>
	<div class="featured-image" style="background-image:url('<?php echo $image[0]; ?>');">
		<div class="overlay"></div>
		<h1><?php the_field('featured_title'); ?></h1>
		<div class="arrowdown"><img src="<?php echo bloginfo('template_url'); ?>/_static/images/arrowdown.png" width="30px"></div>
	</div>

	<div class="inner-wrap">		

		<div class="team">
			<div class="team-flex">
				<?php if( get_field('team') ): $i = 1; ?>
					<?php while( has_sub_field('team') ): ?>
						<div class="team-member">
							<a href="<? the_sub_field('link');?>">
								<div class="image <?php $hover = "team-" . $i; $i++; echo $hover; ?>"></div>
								<style>.<?php echo $hover; ?>{background-image:url("<? the_sub_field('image');?>");transition: background-image 0.5s;}.<?php echo $hover; ?>:hover{background-image:url("<? the_sub_field('image_hover');?>");}</style>
								<h5><? the_sub_field('name');?></h5>
								<p class="title"><? the_sub_field('title');?></p>
							</a>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
		
		<div class="who">
			<?php if( get_field('who_we_are') ): ?>
				<?php while( has_sub_field('who_we_are') ): ?>
					<h4><? the_sub_field('title');?></h4>
					<? the_sub_field('description');?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>		

	</div>

    <?php endwhile; // End of the loop.?>

</div> <!-- /.container -->

<?php get_footer(); ?>
