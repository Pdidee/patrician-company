<?php
/**
 * Default Page Template
 */

get_header();
?>
<div class="container">
<?php while ( have_posts() ) : the_post(); ?>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
		<?php endif; ?>
	<div class="featured-image" style="background-image:url('<?php echo $image[0]; ?>');">
		<div class="overlay"></div>
		<h1><?php the_field('featured_title'); ?></h1>
		<div class="arrowdown"><img src="<?php echo bloginfo('template_url'); ?>/_static/images/arrowdown.png" width="30px"></div>
	</div>

	<div class="inner-wrap">		
		<div class="contact-sub"><?php the_field('subtitle'); ?></div>
		<div class="contact-flex">
			<div class="left">
				<!-- <img src="<?php echo bloginfo('template_url'); ?>/_static/images/patricianlogo2.png"> -->
				<?php the_content(); ?>	
				<ul class="social">
				<?php if( get_field('social_links', 'option') ): ?>
					<?php while( has_sub_field('social_links', 'option') ): ?>

						<li><a href="<? the_sub_field('social_link');?>" target="_blank"><i class="fa <?php the_sub_field('social_media_icon'); ?>" aria-hidden="true"></i></a></li>

					<?php endwhile; ?>
				<?php endif; ?>
				</ul>
			</div>
			<div class="right">
				<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
			</div>
		</div>

	</div>

    <?php endwhile; // End of the loop.?>

</div> <!-- /.container -->

<?php get_footer(); ?>
