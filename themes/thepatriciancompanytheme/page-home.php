<?php
/**
 * Default Page Template
 */

get_header();
?>

<div class="container">  
	<?php while ( have_posts() ) : the_post(); ?>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
		<?php endif; ?>
	<div class="featured-image" style="background-image:url('<?php echo $image[0]; ?>');">
		<div class="overlay"></div>
		<h1><?php the_field('featured_title'); ?></h1>
		<div class="arrowdown"><img src="<?php echo bloginfo('template_url'); ?>/_static/images/arrowdown.png" width="30px"></div>
	</div>
		
	<div class="inner-wrap">
		<div class="who">
			<?php if( get_field('who_we_are') ): ?>
				<?php while( has_sub_field('who_we_are') ): ?>
					<h4><? the_sub_field('title');?></h4>
					<? the_sub_field('description');?>
					
				<?php endwhile; ?>
			<?php endif; ?>	
		</div>		
		<div id="services">
			<h4><?php the_field('services_title'); ?></h4>
			<div class="service-flex">
				<?php if( get_field('services') ): ?>
					<?php while( has_sub_field('services') ): ?>
						<div class="single-service">
							<a href="<?php echo home_url(); ?>/eventbook_category/<? the_sub_field('eventbook_category_link');?>"><div class="image" style="background-image:url('<? the_sub_field('image');?>');"></div></a>
							<h5><a href="<?php echo home_url(); ?>/eventbook_category/<? the_sub_field('eventbook_category_link');?>"><? the_sub_field('title');?></a></h5>
							<div class="description"><? the_sub_field('description');?></div>
					
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>

		<div class="eventbook">
			<h4><?php the_field('eventbook_title'); ?></h4>
				<? $query = new WP_Query( array( 'post_type' => 'eventbook', 'posts_per_page' => -1, 'paged' => $paged ) );
		if ( $query->have_posts() ) : ?>
	<div class="event-listing">
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
			$image = $image[0];?>
			<a href="<? the_permalink(); ?>">
				<div class="singleImage" style="background-image: url('<?php echo $image; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
					<div class="overlay">
						<div class="title"><?php the_title(); ?></div>
					</div>
				</div> 
			</a>
			<?php endwhile; endif;
			wp_reset_query();
			?>
	</div>
		</div>

		<div class="instagram">
			<h4>Get Social</h4>
			<?php echo do_shortcode('[instagram-feed]'); ?>
		</div>

	</div>

    <?php endwhile; // End of the loop.?>
</div> <!-- /.container -->

<?php get_footer(); ?>
