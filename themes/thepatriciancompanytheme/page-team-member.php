<?php
/*
Template Name: Team Member
*/
get_header();
?>

<div class="container">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
		<?php endif; ?>
	<div class="featured-image" style="background-image:url('<?php echo $image[0]; ?>');">
		<div class="overlay"></div>
		<h1><?php the_field('featured_title'); ?><br>
			<div class="span"><?php the_field('featured_subtitle'); ?></div>
		</h1>
		<div class="arrowdown"><img src="<?php echo bloginfo('template_url'); ?>/_static/images/arrowdown.png" width="30px"></div>
	</div>
	
	<div class="inner-wrap">		

	<div class="single-team">
		<div class="team-flex2">
			<?php if( get_field('team_member') ): ?>
				<?php while( has_sub_field('team_member') ): ?>
					<div class="left">
						<div class="image" style=background-image:url('<? the_sub_field('image');?>');></div>
					</div>
					<div class="right">
						<h5><? the_sub_field('name');?></h5>
						<p class="title"><? the_sub_field('title');?></p>
						<div class="about-member"><?php the_sub_field('about'); ?></div>
					</div>				
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>	

	</div>

    <?php endwhile; // End of the loop.?>

</div> <!-- /.container -->

<?php get_footer(); ?>
