<?php
/**
 * Default Page Template
 */

get_header();
?>
<div class="container">
<?php while ( have_posts() ) : the_post(); ?>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
		<?php endif; ?>
	<div class="featured-image" style="background-image:url('<?php echo $image[0]; ?>');">
		<h1><?php the_field('featured_title'); ?></h1>
		<div class="arrowdown"><img src="<?php echo bloginfo('template_url'); ?>/_static/images/arrowdown.png" width="30px"></div>
	</div>

	<div class="inner-wrap">		
		<?php the_content(); ?>
	</div>

	</div>

    <?php endwhile; // End of the loop.?>

</div> <!-- /.container -->

<?php get_footer(); ?>
