<?php
/**
 * The template for displaying all single posts.
 */

get_header();
?>

<div class="container">
   <?php while ( have_posts() ) : the_post(); ?>
        <?php if (has_post_thumbnail( $post->ID ) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
        <?php endif; ?>
    <div class="featured-image" style="background-image:url('<?php echo $image[0]; ?>');">
      <div class="overlay"></div>
        <h1><?php the_title(); ?><br>
            <div class="span"><?php the_field('featured_subtitle'); ?></div>
        </h1>
        <div class="arrowdown"><img src="<?php echo bloginfo('template_url'); ?>/_static/images/arrowdown.png" width="30px"></div>
    </div>
<?php endwhile; ?>
   <?php
    while ( have_posts() ) : the_post(); ?>
     <div class="inner-wrap"></div>
    <div class="gallery">
        <?php the_content(); ?>
    </div>

        <?
    endwhile; // End of the loop.
    ?>
</div> <!-- /.container -->

<?php get_footer(); ?>