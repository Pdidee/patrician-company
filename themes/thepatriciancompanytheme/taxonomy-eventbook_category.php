<?php
/**
 * Custom Taxonomy Template
 */

get_header();
?>
<div class="container">
 <?
		$target_post_id = "109";
		if (has_post_thumbnail($target_post_id) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id($target_post_id), 'full' ); ?>
			<?php endif; ?>
	<div class="featured-image" style="background-image:url('<?php echo $image[0]; ?>');">
		<div class="overlay"></div>
		<h1>
		<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term->name; ?>
			
						
		</h1>
						
						
		<div class="arrowdown"><img src="<?php echo bloginfo('template_url'); ?>/_static/images/arrowdown.png" width="30px"></div>
	</div>
	
<div class="event-listing">
 <?php
	if ( have_posts() ) : 
    while ( have_posts() ) : the_post(); ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
			$image = $image[0];?>
			<a href="<?php the_permalink(); ?>">
				<div class="singleImage" style="background-image: url('<?php echo $image; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
					<div class="overlay">
						<div class="title"><?php the_title(); ?></div>
					</div>
				</div> 
			</a>
			
	
		
		
<?php
    endwhile; endif;
			wp_reset_query(); // End of the loop.
    ?>
    </div>	

</div><!-- .content-area -->


<div class="clearfix"></div>

<?php get_footer(); ?>